import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller implements ActionListener {
    private Model model;

    public Controller(View view) {
        model = new Model(view);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("+")
                || e.getActionCommand().equals("-")
                || e.getActionCommand().equals("/")
                || e.getActionCommand().equals("*")
                || e.getActionCommand().equals("^")) {
            model.setOperand(e.getActionCommand().charAt(0));
        } else if (e.getActionCommand().equals("allClear")) {
            model.allClear();
        } else if (e.getActionCommand().equals("clear")) {
            model.clear();
        } else if (e.getActionCommand().equals("1")
                || e.getActionCommand().equals("2")
                || e.getActionCommand().equals("3")
                || e.getActionCommand().equals("4")
                || e.getActionCommand().equals("5")
                || e.getActionCommand().equals("6")
                || e.getActionCommand().equals("7")
                || e.getActionCommand().equals("8")
                || e.getActionCommand().equals("9")
                || e.getActionCommand().equals("0")) {
            model.addText(e.getActionCommand());
        } else if (e.getActionCommand().equals(",")) {
            model.setFloatPressed();
        } else if (e.getActionCommand().equals("=")) {
            model.saveNum2();
        } else if (e.getActionCommand().equals("!")) {
            model.squareRoot();
        }
    }
}



