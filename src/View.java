import javax.swing.*;
import javax.xml.namespace.QName;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class View {
    private JTextField textField;

    public View(){
        JFrame frame = new JFrame("Калькулятор");

        Controller buttonsHandler = new Controller(this);

        Dimension dimension = new Dimension(320,605);
        Dimension dimensionButton = new Dimension(70,65);
        frame.setSize(dimension);
        frame.setLayout(null);

        textField = new JTextField();
        textField.setBounds(0, 10, 225,130);
        textField.setText("0");
        frame.add(textField);

        int x = 5;
        int y = 150;
        int offset = 5;
        int num = 1;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                String buttonName = String.valueOf(num);
                JButton button = new JButton(buttonName);
                button.setBounds(x, y, dimensionButton.width, dimensionButton.height);
                button.setActionCommand(buttonName);
                button.addActionListener(buttonsHandler);
                button.setBackground(Color.red );
                frame.add(button);
                x += dimensionButton.width + offset;
                num++;
            }
            x = 5;
            y += dimensionButton.height + offset;
        }

        JButton button0 = new JButton("0");
        button0.setBounds(80, 360, dimensionButton.width, dimensionButton.height);
        button0.setActionCommand("0");
        button0.addActionListener(buttonsHandler);
        button0.setBackground(Color.red);
        frame.add(button0);

        JButton buttonC = new JButton("CE");
        buttonC.setBounds(230, 10, dimensionButton.width, dimensionButton.height);
        buttonC.setActionCommand("allClear");
        buttonC.addActionListener(buttonsHandler);
        buttonC.setBackground(Color.white);
        buttonC.setBackground(Color.red);
        frame.add(buttonC);

        JButton buttonClear = new JButton("C");
        buttonClear.setBounds(230, 75, dimensionButton.width, dimensionButton.height);
        buttonClear.setActionCommand("clear");
        buttonClear.addActionListener(buttonsHandler);
        buttonClear.setBackground(Color.red);

        frame.add(buttonClear);


        JButton buttonMinus = new JButton("-");
        buttonMinus.setBounds(230, 220, dimensionButton.width, dimensionButton.height);
        buttonMinus.setActionCommand("-");
        buttonMinus.addActionListener(buttonsHandler);
        buttonMinus.setBackground(Color.red);
        frame.add(buttonMinus);

        JButton buttonPlus = new JButton("+");
        buttonPlus.setBounds(230, 150, dimensionButton.width, dimensionButton.height);
        buttonPlus.setActionCommand("+");
        buttonPlus.addActionListener(buttonsHandler);
        buttonPlus.setBackground(Color.red);
        frame.add(buttonPlus);

        JButton buttonMult = new JButton("*");
        buttonMult.setBounds(230, 290, dimensionButton.width, dimensionButton.height);
        buttonMult.setActionCommand("*");
        buttonMult.addActionListener(buttonsHandler);
        buttonMult.setBackground(Color.red);
        frame.add(buttonMult);

        JButton buttonDivision = new JButton("/");
        buttonDivision.setBounds(230, 360, dimensionButton.width, dimensionButton.height);
        buttonDivision.setActionCommand("/");
        buttonDivision.addActionListener(buttonsHandler);
        buttonDivision.setBackground(Color.red);
        frame.add(buttonDivision);

        JButton buttonX2 = new JButton("X^2");
        buttonX2.setBounds(155, 360, dimensionButton.width, dimensionButton.height);
        buttonX2.setActionCommand("^");
        buttonX2.addActionListener(buttonsHandler);
        buttonX2.setBackground(Color.red);
        frame.add(buttonX2);

        JButton buttonKoren = new JButton("!");
        buttonKoren.setBounds(5, 360, dimensionButton.width, dimensionButton.height);
        buttonKoren.setActionCommand("!");
        buttonKoren.addActionListener(buttonsHandler);
        buttonKoren.setBackground(Color.red);
        frame.add(buttonKoren);

        JButton buttonFloat = new JButton(",");
        buttonFloat.setBounds(5, 430, 295, 65);
        buttonFloat.setActionCommand(",");
        buttonFloat.addActionListener(buttonsHandler);
        frame.add(buttonFloat);

        JButton buttonEquals = new JButton("=");
        buttonEquals.setBounds(5, 500, 295, 65);
        buttonEquals.setActionCommand("=");
        buttonEquals.addActionListener(buttonsHandler);
        frame.add(buttonEquals);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public void UpdateText(String text){
        textField.setText(text);
    }

    public String GetCurrentText(){
        return textField.getText();
    }
}
