public enum Operand {
    PLUS('+'),
    MINUS('-'),
    MULTIPLICATION('*'),
    DIVISION('/'),
    EXPONENTIATION ('^');

    private char operand;

    Operand(char c){
        operand = c;
    }

    public char get(){
        return operand;
    }

}
