public class Model {

    private String currentText = "";
    private int num1 = 0;
    private int num2 = 0;
    private double num1D = 0.0;
    private double num2D = -0.0;
    private char operand = 's';
    private boolean isfloatPressed = false;
    public int floatPressedCount = 0;
    public double result = 0.0;

    private View view;

    public Model(View view){
        this.view = view;
    }

    public void addText(String text){
        currentText = view.GetCurrentText();
        //System.out.println("currentText = " + currentText);
        if(currentText.length() > 0){
            if (currentText.charAt(0) == '0'){
                currentText = "";
            }
        }
        currentText += text;
        updateText();
    }

    private void updateText(){
        view.UpdateText(currentText);
    }

    public void setOperand(char operand){
        this.operand = operand;
        floatPressedCount = 0;
        saveNum1();
        currentText = String.valueOf(operand);
        updateText();
    }

    private void saveNum1(){
        currentText = view.GetCurrentText();
        if(!isfloatPressed){
            num1 = Integer.parseInt(currentText);
        } else {
            num1D = Double.parseDouble(currentText);
        }
        currentText = "";
    }

    public void saveNum2() {
        if (!isfloatPressed) {
            DeleteOperandFromText(currentText);
            num2 = Integer.parseInt(currentText);
        } else {
            DeleteOperandFromText(currentText);
            num2D = Integer.parseInt(currentText);
        }
        calculate();
    }

    public void calculate(){
            checkOperand();
            if (!isfloatPressed) {
                int result2 = (int) result;
                currentText = String.valueOf(result2);
            } else {
                currentText = String.valueOf(result);
            }
        updateText();
    }

    private void checkOperand(){
        switch (operand){
            case '+':
                floatPressedCount = 0;
                if (!isfloatPressed){
                    result = num1 + num2;
                } else {
                    result = num1D + num2D;
                }
                break;
            case '-':
                floatPressedCount = 0;
                if (!isfloatPressed){
                    result = num1 - num2;
                } else {
                    result = num1D - num2D;
                }
                break;
            case '*':
                floatPressedCount = 0;
                if (!isfloatPressed){
                    result = num1 * num2;
                } else {
                    result = num1D * num2D;
                }
                break;
            case '/':
                floatPressedCount = 0;
                if (!isfloatPressed){
                    result = num1 / num2;
                } else {
                    result = num1D / num2D;
                }
                break;
            case '^':
                floatPressedCount = 0;
                result = exponentiation();
                break;
            default:
                result = 0.0;
        }
    }

    public int exponentiation(){
        return (int) Math.pow(num1,num2);
    }

    public void allClear (){
        floatPressedCount = 0;
        isfloatPressed = false;
        currentText = "0";
        view.UpdateText(currentText);
    }

    public void clear() {
        if(currentText.charAt(currentText.length() - 1) == '.'){
            isfloatPressed = false;
        }
        floatPressedCount = 0;
        char[] array = currentText.toCharArray();
        currentText = "";
        for (int i = 0; i < array.length - 1; i++) {
            currentText += array[i];
        }
        if(currentText.length() == 0){
            currentText = "0";
        }
        updateText();
    }

    public void  squareRoot (){
        saveNum1();
        double result = Math.sqrt(num1);
        currentText = String.valueOf(result);
        updateText();
    }

    public void setFloatPressed(){
        floatPressedCount++;
        isfloatPressed = true;
        if(floatPressedCount == 1){
            currentText += ".";
            updateText();
        }
    }

    private void DeleteOperandFromText(String text){
        System.out.println("curr1 " + currentText);
        currentText = "";
        for(int i = 1; i < text.length(); i++){
            currentText += text.charAt(i);
        }
        System.out.println("currentText = " + currentText);
    }
}
